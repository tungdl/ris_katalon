import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://idc.pacs.cf/login')

WebUI.maximizeWindow()

WebUI.sendKeys(findTestObject('LoginPage/txbUsername'), 'admin_tuan')

WebUI.sendKeys(findTestObject('LoginPage/txbPassword'), 'vnpt123')

WebUI.click(findTestObject('LoginPage/btnLogin'))

WebUI.waitForElementVisible(findTestObject('MainPage/menuTiepNhan'), 10)

WebUI.click(findTestObject('MainPage/menuTiepNhan'))

WebUI.waitForElementVisible(findTestObject('RequestPage/btnLuuYeuCau'), 10)

WebUI.selectOptionByValue(findTestObject('RequestPage/slbKieuBenhNhan'), 'RIS', false)

WebUI.selectOptionByValue(findTestObject('RequestPage/slbLoaiBenhNhan'), 'I', false)

WebUI.selectOptionByValue(findTestObject('RequestPage/slbDoiTuong'), '260', false)

WebUI.sendKeys(findTestObject('RequestPage/txbMaBenhNhan'), 'BN00161033')

WebUI.sendKeys(findTestObject('RequestPage/txbTenBenhNhan'), 'Cuong Viet Hoa')

WebUI.sendKeys(findTestObject('RequestPage/txbTuoi'), '21y')

WebUI.selectOptionByValue(findTestObject('RequestPage/slbGioiTinh'), 'MALE', false)

WebUI.sendKeys(findTestObject('RequestPage/txbMaBHYT'), 'DN4481222123343')

WebUI.sendKeys(findTestObject('RequestPage/txbDiaChi'), '344 duong 2-9')

WebUI.click(findTestObject('RequestPage/containerDichVu'))

WebUI.sendKeys(findTestObject('RequestPage/txbDichVuSearch'), '18.0141.0032')

WebUI.waitForElementClickable(findTestObject('RequestPage/valueDichVu'), 30)

WebUI.click(findTestObject('RequestPage/valueDichVu'))

WebUI.click(findTestObject('RequestPage/btnLuuYeuCau'))

WebUI.waitForElementVisible(findTestObject('RequestPage/msgThanhCong'), 30)

WebUI.verifyElementAttributeValue(findTestObject('RequestPage/msgThanhCong'), 'innerText', 'Lưu phiếu chỉ định thành công!', 
    0)

WebUI.closeBrowser()

